package com.bigdatatrainings;

import com.bigdatatrainings.model.CompositeKey;
import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

public class ChannelReducer extends
    Reducer<CompositeKey, IntWritable, CompositeKey, IntWritable> {

  @Override
  protected void reduce(CompositeKey key, Iterable<IntWritable> values, Context context)
      throws IOException, InterruptedException {
    for (IntWritable channel : values) {
      context.write(key, channel);
    }
  }
}