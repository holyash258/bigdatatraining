package com.bigdatatrainings.rdd;

import org.apache.spark.api.java.function.Function;
import scala.Serializable;
import scala.Tuple3;

public class RecordSortFunction implements Serializable,
    Function<Tuple3<String, Integer, String>, Integer> {

  @Override
  public Integer call(Tuple3<String, Integer, String> stringIntegerStringTuple3) throws Exception {
    return stringIntegerStringTuple3._2();
  }
}
