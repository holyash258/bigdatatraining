package com.bigdatatraining;

import java.sql.Timestamp;
import scala.Serializable;

public class UserState implements Serializable {

  private String username;
  private String activity;
  private Timestamp start;
  private Timestamp end;

  public UserState(String username, String activity, Timestamp start, Timestamp end) {
    this.username = username;
    this.activity = activity;
    this.start = start;
    this.end = end;
  }

  public UserState() {
  }

  @Override
  public String toString() {
    return username + activity + start + end;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getActivity() {
    return activity;
  }

  public void setActivity(String activity) {
    this.activity = activity;
  }

  public Timestamp getStart() {
    return start;
  }

  public void setStart(Timestamp start) {
    this.start = start;
  }

  public Timestamp getEnd() {
    return end;
  }

  public void setEnd(Timestamp end) {
    this.end = end;
  }
}
