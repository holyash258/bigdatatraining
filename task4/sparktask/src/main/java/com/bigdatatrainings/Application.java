package com.bigdatatrainings;

import com.bigdatatrainings.datset.ParserDataSet;
import com.bigdatatrainings.rdd.ParserRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class Application {

  private static SparkSession spark;

  public static void main(String[] args) {
    spark = SparkSession
        .builder()
        .appName("hockey")
        .config("spark.master", "local")
        .getOrCreate();
    final Dataset<Row> masters = spark.read()
        .option("header", true)
        .csv(args[0])
        .select("playerID", "firstName", "lastName");
    final Dataset<Row> goalies = spark.read()
        .option("header", true)
        .csv(args[1])
        .select("playerID", "tmID", "PostGA");
    final Dataset<Row> teams = spark.read()
        .option("header", true)
        .csv(args[2])
        .select("tmID", "name");
    parseWithDataSetAndSQL(masters, goalies, teams);
    parseWithRDD(args[0], args[1], args[2]);
    Dataset<Row> sqlResult = parseSQL(masters, goalies, teams);
    sqlResult.limit(10)
        .repartition(1)
        .write().option("header", true).csv("/user/dimamedison/spark-task/sql");
    spark.stop();
  }

  private static void parseWithDataSetAndSQL(Dataset<Row> masters, Dataset<Row> goalies,
      Dataset<Row> teams) {
    ParserDataSet parserDataSet = new ParserDataSet(goalies, masters, teams);
    parserDataSet.parse();
  }

  private static Dataset<Row> parseSQL(Dataset<Row> masters, Dataset<Row> goalies,
      Dataset<Row> teams) {
    masters.createOrReplaceTempView("master");
    goalies.createOrReplaceTempView("goalies");
    teams.createOrReplaceTempView("team");
    spark.sql("select playerID, sum(postGA) as goals"
        + " from goalies"
        + " group by playerID")
        .createOrReplaceTempView("idAndGoals");
    spark.sql("select master.*, goals "
        + " from master"
        + " join idAndGoals on master.playerID = idAndGoals.playerID")
        .createOrReplaceTempView("masterGoals");
    spark.sql("select distinct goalies.playerID, team.name"
        + " from goalies"
        + " join team on goalies.tmID = team.tmID"
        + " order by playerID")
        .createOrReplaceTempView("idAndTeamName");
    spark.sql("select playerID, concat_ws(\", \", collect_list(name)) as teams"
        + " from idAndTeamName"
        + " group by playerID")
        .createOrReplaceTempView("idAndTeams");
    return spark.sql("select firstName, lastName, goals, teams"
        + " from masterGoals"
        + " join idAndTeams on masterGoals.playerID = idAndTeams.playerID"
        + " order by goals desc");
  }

  private static void parseWithRDD(String path1, String path2, String path3) {
    final JavaRDD<String> master = spark.read()
        .textFile(path1).javaRDD()
        .filter(s -> !s.startsWith("playerID"));
    final JavaRDD<String> goalies = spark.read()
        .textFile(path2).javaRDD()
        .filter(s -> !s.startsWith("playerID"));
    final JavaRDD<String> teams = spark.read()
        .textFile(path3).javaRDD()
        .filter(s -> !s.startsWith("year"));
    ParserRDD parserRDD = new ParserRDD(master, goalies, teams);
    parserRDD.parse();
  }

}

