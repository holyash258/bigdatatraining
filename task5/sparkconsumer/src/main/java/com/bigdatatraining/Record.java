package com.bigdatatraining;

import java.sql.Timestamp;
import scala.Serializable;

public class Record implements Serializable {

  private String username;
  private String activity;
  private Timestamp start;

  public Record(String username, String activity, Timestamp start) {
    this.username = username;
    this.activity = activity;
    this.start = start;
  }

  @Override
  public String toString() {
    Timestamp none = new Timestamp(6284160L);
    return String
        .format("%-20s%-20s%-30s", username, activity, start);
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getActivity() {
    return activity;
  }

  public void setActivity(String activity) {
    this.activity = activity;
  }

  public Timestamp getStart() {
    return start;
  }

  public void setStart(Timestamp start) {
    this.start = start;
  }

  public Record() {
  }
}
