package com.bigdatatrainings.comparator;

import com.bigdatatrainings.model.CompositeKey;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class GroupComparator extends WritableComparator {
  protected GroupComparator() {
    super(CompositeKey.class, true);
  }

  @Override
  public int compare(WritableComparable a, WritableComparable b) {
    CompositeKey c1 = (CompositeKey) a;
    CompositeKey c2 = (CompositeKey) b;
    return c1.getHotelId().compareTo(c2.getHotelId());
  }
}
