name := "sparkconsumerelastic"

version := "0.1"

scalaVersion := "2.11.12"

libraryDependencies += "org.apache.spark" %% "spark-core" % "2.3.3"

libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.3.3"

libraryDependencies += "org.apache.spark" %% "spark-streaming" % "2.3.3"

libraryDependencies += "org.apache.spark" %% "spark-sql-kafka-0-10" % "2.3.3"

libraryDependencies += "org.apache.kafka" % "kafka-clients" % "2.4.0"

libraryDependencies += "org.elasticsearch" %% "elasticsearch-spark-20" % "7.5.0"

