package com.bigdatatrainings.comparator;

import com.bigdatatrainings.model.CompositeKey;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class KeyComparator extends WritableComparator {
  protected KeyComparator() {
    super(CompositeKey.class, true);
  }

  @Override
  public int compare(WritableComparable a, WritableComparable b) {
    CompositeKey c1 = (CompositeKey) a;
    CompositeKey c2 = (CompositeKey) b;
    int cmp = c1.getHotelId().compareTo(c2.getHotelId());
    if (cmp != 0) {
      return cmp;
    }
    cmp = c1.getSrchCi().compareTo(c2.getSrchCi());
    if (cmp != 0) {
      return cmp;
    }
    return c1.getId().compareTo(c2.getId());
  }
}
