import com.bigdatatrainings.ChannelReducer;
import com.bigdatatrainings.HotelChannelMapper;
import com.bigdatatrainings.model.CompositeKey;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.Before;
import org.junit.Test;

public class MapReduceTest {

  MapDriver<LongWritable, Text, CompositeKey, IntWritable> mapDriver;
  ReduceDriver<CompositeKey, IntWritable, CompositeKey, IntWritable> reduceDriver;
  MapReduceDriver<LongWritable, Text, CompositeKey, IntWritable, CompositeKey, IntWritable> mapReduceDriver;

  @Before
  public void setUp() {
    HotelChannelMapper mapper = new HotelChannelMapper();
    ChannelReducer reducer = new ChannelReducer();
    mapDriver = MapDriver.newMapDriver(mapper);
    reduceDriver = ReduceDriver.newReduceDriver(reducer);
    mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
  }

  @Test
  public void testMapper() throws IOException {
    mapDriver.withInput(new LongWritable(), new Text(
        "18,2015-08-18 12:37:10,2,3,57,342,5021,null,57,0,0,5,2017-09-19,2017-09-20,3,0,1,8243,1,2826088480774"));
    mapDriver.withOutput(new CompositeKey(18L, new Text("2017-09-19"), 2826088480774L),
        new IntWritable(5));

    mapDriver.runTest();
  }

  @Test
  public void testReducer() throws IOException {
    List<IntWritable> values = new ArrayList<>();
    values.add(new IntWritable(5));
    values.add(new IntWritable(4));
    CompositeKey key = new CompositeKey(18L, new Text("2017-09-19"), 2826088480774L);
    reduceDriver.withInput(key, values);
    List<Pair<CompositeKey, IntWritable>> output = new ArrayList<>();
    output.add(new Pair<>(key, new IntWritable(5)));
    output.add(new Pair<>(key, new IntWritable(4)));
    reduceDriver.withAllOutput(output);
    reduceDriver.runTest();
  }

  @Test
  public void testMapReduce() throws IOException {
    List<Pair<LongWritable, Text>> input = new ArrayList<>();
    input.add(new Pair<LongWritable, Text>(new LongWritable(), new Text(
        "18,2015-08-18 12:37:10,2,3,57,342,5021,null,57,0,0,5,2017-09-19,2017-09-20,3,0,1,8243,1,2826088480774")));
    input.add(new Pair<LongWritable, Text>(new LongWritable(), new Text(
        "18,2015-08-18 12:37:10,2,3,57,342,5021,null,57,0,0,4,2017-09-19,2017-09-20,3,0,1,8243,1,2826088480774")));
    mapReduceDriver.withAll(input);
    List<IntWritable> values = new ArrayList<>();
    values.add(new IntWritable(5));
    values.add(new IntWritable(4));
    CompositeKey key = new CompositeKey(18L, new Text("2017-09-19"), 2826088480774L);
    List<Pair<CompositeKey, IntWritable>> output = new ArrayList<>();
    output.add(new Pair<>(key, new IntWritable(5)));
    output.add(new Pair<>(key, new IntWritable(4)));
    mapReduceDriver.withAllOutput(output);
    mapReduceDriver.runTest();
  }

}
