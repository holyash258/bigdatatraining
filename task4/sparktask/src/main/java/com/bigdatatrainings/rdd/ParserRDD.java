package com.bigdatatrainings.rdd;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import scala.Tuple2;
import scala.Tuple3;

public class ParserRDD {

  final private JavaRDD<String> master;
  final private JavaRDD<String> goalies;
  final private JavaRDD<String> teams;

  public ParserRDD(JavaRDD<String> master,
      JavaRDD<String> goalies, JavaRDD<String> teams) {
    this.master = master;
    this.goalies = goalies;
    this.teams = teams;
  }

  public void parse() {
    final JavaPairRDD<String, String> playerIdAndName = this.getPlayerIdAndName();
    final JavaPairRDD<String, Integer> playerIdAndGoals = this.getPlayerIdAndGoals();
    final JavaPairRDD<String, String> teamIdAndPlayerId = this.getTeamIdAndPlayerId();
    final JavaPairRDD<String, String> teamIdAndTeamName = this.getTeamIdAndTeamName();
    final JavaPairRDD<String, Integer> playerIdAndGoalsSum = this
        .getPlayerIdAndGoalsSum(playerIdAndGoals);
    final JavaPairRDD<String, Tuple2<String, String>> joinedPlayerIdAndTeamName = teamIdAndPlayerId
        .join(teamIdAndTeamName);
    final JavaPairRDD<String, String> playerIdAndTeamName = joinedPlayerIdAndTeamName
        .mapToPair(row -> new Tuple2<>(row._2._1, row._2._2));
    final JavaPairRDD<String, String> playerIdAndTeamNames = this
        .getPlayerIdAndTeamNames(playerIdAndTeamName);
    final JavaPairRDD<String, Tuple2<String, Integer>> joinedPlayerNameGoals = playerIdAndName
        .join(playerIdAndGoalsSum);
    final JavaPairRDD<String, Tuple2<Tuple2<String, Integer>, String>> joinedPlayerNameGoalsTeams =
        joinedPlayerNameGoals.join(playerIdAndTeamNames);
    final JavaRDD<Tuple3<String, Integer, String>> result = joinedPlayerNameGoalsTeams
        .map(tuple -> new Tuple3<>(tuple._2._1._1, tuple._2._1._2, tuple._2._2));
    result.sortBy(new RecordSortFunction(), false, result.partitions().size())
        .repartition(1)
        .saveAsTextFile("/user/dimamedison/spark-task/rdd");
  }

  private JavaPairRDD<String, String> getPlayerIdAndName() {
    return master.mapToPair(line -> {
      String[] fields = line.split(",");
      return new Tuple2<>(fields[0], String.join(" ", fields[3], fields[4]));
    });
  }

  private JavaPairRDD<String, Integer> getPlayerIdAndGoals() {
    return goalies.mapToPair(line -> {
      line = line.replaceAll(",", ",*");
      String[] fields = line.split(",");
      int goals;
      if (fields[21].equals("*")) {
        goals = 0;
      } else {
        goals = Integer.parseInt(fields[21].substring(1));
      }
      return new Tuple2<>(fields[0], goals);
    });
  }

  private JavaPairRDD<String, String> getTeamIdAndPlayerId() {
    return goalies.mapToPair(line -> {
      String[] fields = line.split(",");
      return new Tuple2<>(fields[3], fields[0]);
    });
  }

  private JavaPairRDD<String, String> getTeamIdAndTeamName() {
    return teams.mapToPair(line -> {
      String[] fields = line.split(",");
      return new Tuple2<>(fields[2], fields[18]);
    });
  }

  private JavaPairRDD<String, Integer> getPlayerIdAndGoalsSum(
      final JavaPairRDD<String, Integer> pig) {
    return pig.reduceByKey(Integer::sum);
  }

  private JavaPairRDD<String, String> getPlayerIdAndTeamNames(
      JavaPairRDD<String, String> playerIdAndTeamName) {
    return playerIdAndTeamName
        .groupByKey()
        .mapToPair(stringIterableTuple2 -> {
          StringBuilder teams = new StringBuilder();
          for (String team : stringIterableTuple2._2
          ) {
            if (!teams.toString().contains(team)) {
              teams.append(team).append("; ");
            }
          }
          return new Tuple2<>(stringIterableTuple2._1, teams.toString());
        });
  }
}
