package com.bigdatatrainings;

import com.bigdatatrainings.model.CompositeKey;
import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class HotelChannelMapper extends
    Mapper<LongWritable, Text, CompositeKey, IntWritable> {

  @Override
  protected void map(LongWritable key, Text value, Context context)
      throws IOException, InterruptedException {
    String[] record = value.toString().split(",");
    int adults = Integer.parseInt(record[14]);
    if (adults > 2) {
      context.write(new CompositeKey(Long.parseLong(record[0]), new Text(record[12]),
          Long.parseLong(record[19])), new IntWritable(Integer.parseInt(record[11])));
    }
  }
}
