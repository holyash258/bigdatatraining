package com.bigdatatraining;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Properties;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class Application {

  public static void main(String[] args) {
    Properties kafkaProps = new Properties();
    kafkaProps
        .put("bootstrap.servers", "localhost:9092");
    kafkaProps.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
    kafkaProps.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

    KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(kafkaProps);
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    try {
      while (true) {
        StringBuilder sb = new StringBuilder();
        sb.append(br.readLine()) //username
            .append(",")
            .append(br.readLine()) //activity
            .append(",")
            .append(new Date().getTime());
        ProducerRecord<String, String> record = new ProducerRecord<>("test", sb.toString());
        kafkaProducer.send(record);
      }
    } catch (IOException e) {
      System.out.println("Wrong input");
    } finally {
      kafkaProducer.flush();
      kafkaProducer.close();
    }
  }
}
