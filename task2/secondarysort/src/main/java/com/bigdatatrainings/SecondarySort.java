package com.bigdatatrainings;

import com.bigdatatrainings.comparator.GroupComparator;
import com.bigdatatrainings.comparator.KeyComparator;
import com.bigdatatrainings.model.CompositeKey;
import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class SecondarySort {

  public static class HotelPartitioner extends Partitioner<CompositeKey, IntWritable> {

    @Override
    public int getPartition(CompositeKey compositeKey, IntWritable intWritable, int i) {
      return (int) (Math.abs(compositeKey.getHotelId() * 127) % i);
    }
  }

  public static void main(String[] args)
      throws IOException, ClassNotFoundException, InterruptedException {
    Configuration configuration = new Configuration();
    Job job = Job.getInstance(configuration, "word count");
    job.setJarByClass(SecondarySort.class);
    job.setMapperClass(HotelChannelMapper.class);
    job.setPartitionerClass(HotelPartitioner.class);
    job.setSortComparatorClass(KeyComparator.class);
    job.setGroupingComparatorClass(GroupComparator.class);
    job.setReducerClass(ChannelReducer.class);
    job.setOutputKeyClass(CompositeKey.class);
    job.setOutputValueClass(IntWritable.class);
    for (int i = 0; i < args.length - 1; i++) {
      FileInputFormat.addInputPath(job, new Path(args[i]));
    }
    FileOutputFormat.setOutputPath(job, new Path(args[args.length - 1]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }

}
