package com.bigdatatraining

import java.sql.Timestamp

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.streaming.{GroupState, GroupStateTimeout, OutputMode}

import scala.collection.mutable.ListBuffer

object Application {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("Consumer")
      .master("local")
      .config("es.nodes", "localhost")
      .config("es.port", "9200")
      .config("es.net.http.auth.user", "elastic")
      .config("es.net.http.auth.pass", "changeme")
      .getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    val df = spark.readStream
      .format("kafka")
      .option("subscribe", "activities")
      .option("kafka.bootstrap.servers", "localhost:9092")
      .load()

    import spark.implicits._
    val rowRecords = df.selectExpr("CAST(value as STRING)")
      .selectExpr(
        "SPLIT(value, ',')[0] as username",
        "SPLIT(value, ',')[1] as activity",
        "CAST(SPLIT(value, ',')[2] as timestamp) as start"
      )

    val recordDS = rowRecords.as[Record]
    recordDS.withWatermark("start", "1 minutes")
      .groupByKey(_.username)
      .flatMapGroupsWithState(OutputMode.Append(),
        GroupStateTimeout.EventTimeTimeout)(updateUserActivities)
      .writeStream
      .outputMode("append")
      .format("org.elasticsearch.spark.sql")
      .option("checkpointLocation", "logs")
      .start("activities/_doc")
      .awaitTermination()
    spark.stop()
  }

  def updateUserActivities(user: String, iterator: Iterator[Record], oldState: GroupState[UserState]):
  Iterator[UserState] = {
    val result = ListBuffer[UserState]()
    val newRecords = iterator.toList.sortBy(_.start.getTime)
    if (oldState.exists) {
      val state = oldState.get
      result += UserState(state.username, state.activity, state.start, newRecords.head.start)
    }
    for (i <- 0 to newRecords.length - 2) {
      result += UserState(newRecords(i).username, newRecords(i).activity,
        newRecords(i).start, newRecords(i + 1).start)
    }
    val last = newRecords.last
    oldState.update(UserState(last.username, last.activity, last.start, new Timestamp(0L)))
    result.iterator
  }
}

case class Record(username: String, activity: String, start: Timestamp)

case class UserState(username: String, activity: String, start: Timestamp, end: Timestamp)
