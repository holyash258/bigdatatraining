package com.bigdatatrainings.datset;

import static org.apache.spark.sql.functions.collect_list;
import static org.apache.spark.sql.functions.concat_ws;
import static org.apache.spark.sql.functions.desc;
import static org.apache.spark.sql.functions.sum;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

public class ParserDataSet {

  private final Dataset<Row> goalies;
  private final Dataset<Row> masters;
  private final Dataset<Row> teams;

  public ParserDataSet(Dataset<Row> goalies,
      Dataset<Row> masters, Dataset<Row> teams) {
    this.goalies = goalies;
    this.masters = masters;
    this.teams = teams;
  }

  public void parse() {
    Dataset<Row> result = getResult();
    result.limit(10)
        .repartition(1)
        .write().option("header", true).csv("/user/dimamedison/spark-task/dataset");
  }

  private Dataset<Row> getResult() {
    Dataset<Row> playerIdAndGoalsSum = goalies.groupBy("playerID").agg(sum("PostGA")).toDF()
        .withColumnRenamed("sum(PostGA)", "goals");
    Dataset<Row> masterGoals = masters
        .join(playerIdAndGoalsSum, "playerID")
        .orderBy(desc("goals"));
    Dataset<Row> playerIdAndTeamId = goalies.select("playerID", "tmID");
    Dataset<Row> teamIdAndTeamName = teams.select("tmID", "name");
    Dataset<Row> playerIdAndTeamName = playerIdAndTeamId.join(teamIdAndTeamName, "tmID")
        .distinct();
    Dataset<Row> playerIdAndTeams = playerIdAndTeamName.select("playerID", "name")
        .groupBy("playerID")
        .agg(concat_ws(", ", collect_list("name")))
        .withColumnRenamed("concat_ws(, , collect_list(name))", "teams");
    return masterGoals
        .join(playerIdAndTeams, "playerID")
        .orderBy(desc("goals"));
  }

}
