package com.bigdatatrainings.model;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

public class CompositeKey implements WritableComparable<CompositeKey> {
  private Long id;
  private Text srchCi;
  private Long hotelId;


  public CompositeKey() {
    this(0L, new Text(), 0L);
  }

  public CompositeKey(Long id, Text srchCi, Long hotelId) {
    this.id = id;
    this.srchCi = srchCi;
    this.hotelId = hotelId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CompositeKey that = (CompositeKey) o;
    return id.equals(that.id) &&
        srchCi.equals(that.srchCi) &&
        hotelId.equals(that.hotelId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, srchCi, hotelId);
  }

  public Long getId() {
    return id;
  }

  public Text getSrchCi() {
    return srchCi;
  }

  public Long getHotelId() {
    return hotelId;
  }

  @Override
  public String toString() {
    return "CompositeKey{" +
        "id=" + id +
        ", srchCi=" + srchCi +
        ", hotelId=" + hotelId +
        '}';
  }

  @Override
  public int compareTo(CompositeKey o) {
    return 0;
  }

  @Override
  public void write(DataOutput dataOutput) throws IOException {
    dataOutput.writeLong(id);
    srchCi.write(dataOutput);
    dataOutput.writeLong(hotelId);
  }

  @Override
  public void readFields(DataInput dataInput) throws IOException {
    id = dataInput.readLong();
    srchCi.readFields(dataInput);
    hotelId = dataInput.readLong();
  }
}
