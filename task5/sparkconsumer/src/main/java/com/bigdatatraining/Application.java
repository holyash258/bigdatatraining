package com.bigdatatraining;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import org.apache.spark.api.java.function.FlatMapGroupsWithStateFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.streaming.GroupStateTimeout;
import org.apache.spark.sql.streaming.OutputMode;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.spark.sql.streaming.Trigger;

public class Application {

  public static void main(String[] args) throws StreamingQueryException {
    SparkSession spark = SparkSession
        .builder()
        .appName("SparkConsumer")
        .config("spark.master", "local")
        .getOrCreate();

    spark.sparkContext().setLogLevel("ERROR");
    Dataset<Row> df = spark
        .readStream()
        .format("kafka")
        .option("kafka.bootstrap.servers", "localhost:9092")
        .option("subscribe", "test")
        .option("startingOffsets", "earliest")
        .load();

    MapFunction<String, Record> mapToRecord = line -> {
      String[] values = line.split(",");
      String name = values[0];
      String activity = values[1];
      long start = Long.parseLong(values[2]);
      return new Record(name, activity, new Timestamp(start));
    };

    Dataset<Row> rowRecord = df.selectExpr("CAST(value AS STRING)");

    Dataset<Record> records = rowRecord
        .as(Encoders.STRING())
        .map(mapToRecord, Encoders.bean(Record.class));

    records.groupBy(functions.window(records.col("start"), "5 minutes", "5 minutes"),
        records.col("username"))
        .count()
        .sort("window")
        .writeStream()
        .outputMode("complete")
        .format("console")
        .option("truncate", false)
        .trigger(Trigger.ProcessingTime(300000L))
        .start();

    FlatMapGroupsWithStateFunction<String, Record, UserState, UserState> userActivities =
        (FlatMapGroupsWithStateFunction<String, Record, UserState, UserState>) (s, iterator, groupState) -> {
          List<UserState> resultDS = new ArrayList<>();
          List<Record> input = new ArrayList<>();
          while (iterator.hasNext()) {
            input.add(iterator.next());
          }
          input.sort(Comparator.comparing(Record::getStart));
          int i = 0;
          if (groupState.exists()) {
            UserState state = groupState.get();
            resultDS.add(new UserState(state.getUsername(), state.getActivity(), state.getStart(),
                input.get(0).getStart()));
            i = 1;
          }
          for (; i < input.size() - 1; i++) {
            Record current = input.get(i);
            resultDS.add(
                new UserState(current.getUsername(), current.getActivity(), current.getStart(),
                    input.get(i + 1).getStart()));
          }
          Record lastRecord = input.get(input.size() - 1);
          UserState last = new UserState(lastRecord.getUsername(), lastRecord.getActivity(),
              lastRecord.getStart(),
              null);
          resultDS.add(last);
          groupState.update(last);
          return resultDS.iterator();
        };

    records.withWatermark("start", "2 minutes")
        .groupByKey((MapFunction<Record, String>) Record::getUsername, Encoders.STRING())
        .flatMapGroupsWithState(userActivities, OutputMode.Update(), Encoders.bean(UserState.class),
            Encoders.bean(UserState.class), GroupStateTimeout.EventTimeTimeout())
        .writeStream()
        .option("truncate", false)
        .format("console")
        .outputMode("update")
        .start()
        .awaitTermination();
    spark.stop();
  }
}
